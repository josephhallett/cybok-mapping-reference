\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{cybok-ka}[2019/10/03 CyBOK KA class]

\LoadClass[12pt]{article}
\RequirePackage{cybok4}

% Headers and footers
\RequirePackage{fancyhdr}
\pagestyle{fancy}
\fancyhf{}

\newcommand{\katitle}[1]{UNDEFINED}
\newcommand{\kaversion}[1]{UNDEFINED}
\newcommand{\kaeditor}[1]{UNDEFINED}
\newcommand{\kareviewer}[1]{UNDEFINED}

\lhead{
  \noindent\colorbox{CyRed}{\strut\parbox[c]{1.25cm}{}}
  \scriptsize{   \rbtmd{The Cyber Security Body Of Knowledge}}\\
  \vspace{-0.25cm}
  \noindent\colorbox{CyRed}{\strut\parbox[c]{1.7cm}{}}
  \Cyhref[CyGrey]{https://www.cybok.org}{   www.cybok.org}
}

\rhead{\includegraphics[width=2cm]{cybok_logo}}
\rfoot{\footnotesize\rbtb{Page \thepage}}

\lfoot{
  \noindent
  \colorbox{CyRed}{\strut\parbox[c]{0.7cm}{}}
  \scriptsize{   \rbtmd{\if@mainmatter{\katype{ }}\else{}\fi\katitle} \color{CyGrey}{ $\vert{}$ February 2020}}
}

\renewcommand{\headrulewidth}{0.5pt}
\renewcommand{\headrule}{\hbox to\headwidth{\color{separator}\leaders\hrule height \headrulewidth\hfill}}

\renewcommand{\footrulewidth}{0.5pt}
\renewcommand{\footrule}{{\color{separator}\vskip-\footruleskip\vskip-\footrulewidth \hrule width\headwidth height\footrulewidth\vskip\footruleskip}}

% KA copyright notice
\newcommand{\kacopyright}[0]{
  \section*{Copyright}
  
  © Crown Copyright, The National Cyber Security Centre 2020. This information is licensed under the Open Government Licence v3.0. To view this licence, visit:\\{\rbtb{\Cyhref[CyBlack]{http://www.nationalarchives.gov.uk/doc/open-government-licence/}{http://www.nationalarchives.gov.uk/doc/open-government-licence/}} \includegraphics[width=0.65cm]{ogl_logo}}
  
  When you use this information under the Open Government Licence, you should include the following attribution: CyBOK © Crown Copyright, The National Cyber Security Centre 2018, licensed under the Open Government Licence: {\rbtb{\Cyhref[CyBlack]{http://www.nationalarchives.gov.uk/doc/open-government-licence/}{http://www.nationalarchives.gov.uk/doc/open-government-licence/}}}.
  
  The CyBOK project would like to understand how the CyBOK is being used and its uptake. The project would like organisations using, or intending to use, CyBOK for the purposes of education, training, course development, professional development etc. to contact it at {\rbtb{   \Cyhref[CyBlack]{mailto:contact@cybok.org}{contact@cybok.org}}} to let the project know how they are using CyBOK.
  
  \newpage
}

% Load the Author and Editor and Reviewer files
\renewcommand{\kaauthor}[0]{\input{author}}
\renewcommand{\kaeditor}[0]{\input{editor}}
\renewcommand{\kareviewer}[0]{\input{reviewer}}

% KA title page
\RequirePackage{ifthen}
\newcommand{\katitlepage}[0]{
  \begin{titlepage}

    \vspace*{0.5cm}
    
    \fontsize{45}{45}\rbtcb{\begin{flushleft}\katitle{}\\Knowledge Area\\\textcolor{CyRed}{Issue~\kaversion{}}\\\end{flushleft}}
    % AUTHOR %
    {\LARGE \kaauthor{}}
    \vfill
    % EDITOR %
    \begin{flushleft}
      \large
      \textcolor{CyRed}{\rbtb{EDITOR}}\\
      \kaeditor{}
      \vspace{0.5cm} 

      % REVIEWERS %  
      \textcolor{CyRed}{\rbtb{REVIEWERS}}\\
      \kareviewer{}
    \end{flushleft} 
    
    \thispagestyle{empty}
  \end{titlepage}
}

% Set KA cross-referencing style
\renewcommand{\kaxref}[3]{\kaxrefka{#1}{#2}{#3}}