import csv
from collections import defaultdict

def str_to_code(string):
    "Tidy up the string codes if needed"
    if string == 'ADVERSARIAL BEHAVIORS':
        return 'AB'
    if string == 'AUTHENTICATION, AUTHORIZATION, AND ACCOUNTABILITY':
        return 'AAA'
    if string == 'CRYPTOGRAPHY':
        return 'C'
    if string == 'CYBER-PHYSICAL SYSTEMS SECURITY':
        return 'CPS'
    if string == 'DISTRIBUTED SYSTEMS SECURITY':
        return 'DSS'
    if string == 'FORENSICS':
        return 'F'
    if string == 'HARDWARE SECURITY':
        return 'HS'
    if string == 'HUMAN FACTORS':
        return 'HF'
    if string == 'LAW AND REGULATION':
        return 'LR'
    if string == 'MALWARE AND ATTACK TECHNOLOGIES':
        return 'MAT'
    if string == 'NETWORK SECURITY':
        return 'NS'
    if string == 'OPERATING SYSTEMS AND VIRTUALIZATION SECURITY':
        return 'OSV'
    if string == 'PHYSICAL LAYER AND TELECOMMUNICATIONS SECURITY':
        return 'PLS'
    if string == 'PRIVACY AND ONLINE RIGHTS':
        return 'POR'
    if string == 'RISK MANAGEMENT AND GOVERNANCE':
        return 'RMG'
    if string == 'SECURE SOFTWARE LIFECYCLE':
        return 'SSL'
    if string == 'SECURITY OPERATIONS AND INCIDENT MANAGEMENT':
        return 'SO'
    if string == 'SOFTWARE SECURITY':
        return 'SS'
    if string == 'WEB AND MOBILE SECURITY':
        return 'WAM'
    else:
        return string

reference = {}

with open('mega-list.csv', 'r') as f:
    reader = csv.reader(f, dialect='excel')
    for row in reader:
        if row[0] == []:
            continue

        if row[0] in reference:
            index = reference[row[0]]
        else:
            index = set()

        for mapping in row[1].split(';'):
            mapping = str_to_code(mapping)
            index.add(mapping)
        reference[row[0]] = index

with open('cleaned-mega-list.csv', 'w+') as f:
    writer = csv.writer(f, dialect='excel')
    for k, v in reference.items():
        writer.writerow([k, ';'.join(v)])
