import collections
from csv import reader
import re
import sys

_cybok_codes = collections.defaultdict(lambda: '')

def str_to_code(string):
    if string == 'AB':
        return '\\kaAB{}'
    if string == 'AAA':
        return '\\kaAAA{}'
    if string == 'C':
        return '\\kaC{}'
    if string == 'CPS':
        return '\\kaCPS{}'
    if string == 'DSS':
        return '\\kaDSS{}'
    if string == 'F':
        return '\\kaF{}'
    if string == 'HS':
        return '\\kaHS{}'
    if string == 'HF':
        return '\\kaHF{}'
    if string == 'LR':
        return '\\kaLR{}'
    if string == 'MAT':
        return '\\kaMAT{}'
    if string == 'NS':
        return '\\kaNS{}'
    if string == 'OSV':
        return '\\kaOSV{}'
    if string == 'PLS':
        return '\\kaPLS{}'
    if string == 'POR':
        return '\\kaPOR{}'
    if string == 'RMG':
        return '\\kaRMG{}'
    if string == 'SSL':
        return '\\kaSSL{}'
    if string == 'SO':
        return '\\kaSOIM{}'
    if string == 'SS':
        return '\\kaSS{}'
    if string == 'WAM':
        return '\\kaWAM{}'
    
def csv_to_tex(path):
    output_path = re.compile('.csv$').sub('.tex', path)
    with open(path) as f:
        lines = reader(f)
        with open(output_path, 'w+') as out:
            for line in lines:
                key = re.compile('&').sub('\\&', line[0])
                mapping = ' '.join(list(map(str_to_code, re.compile('\s*;\s*').split(line[1]))))
                outstring = f'{key} \\dotfill {mapping} \\\\'
                print(outstring)
                out.write(outstring+"\n")
                
            

if __name__=="__main__":
    csv = sys.argv[1]
    csv_to_tex(csv)
